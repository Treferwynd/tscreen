# tscreen #

Fork of [imgur-screenshot](https://github.com/jomo/imgur-screenshot) with a rofi dialog and a yad confirmation dialog

Usage
----
```bash
imgur-screenshot [ [-c | --connect] | --check | [-v | --version] | [-h | --help] ] |  
                 [ [-o | --open=true|false] [-e | --edit=true|false] [-l | --login=true|false] 
                 [-s | --select=true|false] [-w | --window=true|false] [-m | --multidisp=true|false] 
                 [-k | --keep_file=true|false] [file] ]
```

| short | command                  | description                                                    |
| :---- | -----------------------: | :------------------------------------------------------------- |
| -h    | --help                   | Show help, exit                                                |
| -v    | --version                | Print current version, exit                                    |
|       | --check                  | Check if all dependencies are installed, exit                  |
| -c    | --connect                | Show connected imgur account, exit                             |
| -o    | --open=true⎮false        | override *open* config <br> -o is equal to --open=true         |
| -e    | --edit=true⎮false        | override *edit* config <br> -e is equal to --edit=true         |
| -l    | --login=true⎮false       | override *login* config <br> -lis equal to --login=true        |
| -s    | --select=true⎮false      | interactively choose a window or rectangle with the mouse      |
| -w    | --window=true⎮false      | use the currently focused window                               |
| -m    | --multidisp=true⎮false   | for multiple heads, grab shot from each and join them together |
| -k    | --keep_file=true⎮false   | override *keep_file* config                                    |
|       | *file*                   | instead of uploading a screenshot, upload *file*               |

# Dependencies
* curl
* grep
* libnotify-bin
* rofi
* scrot
* xclip *(needed for `copy_url`)*
* [yad](http://sourceforge.net/projects/yad-dialog/)

# For other stuff
Just check [imgur-screenshot](https://github.com/jomo/imgur-screenshot)
